import React, { useState, useEffect } from "react";
import { GroupOutlined, NumberOutlined } from "@ant-design/icons";
import { Button, Divider, Flex, Input, Modal, Form } from "antd";
import ColorDropDown from "../../DropDown/ColorDropDown";
import ProjectsDropDown from "../../DropDown/ProjectsDropDown";
import ToggleFavorite from "../../Toggle/ToggleFavorite";

const AddOption = () => {
  const [form] = Form.useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [clientReady, setClientReady] = useState(false);

  useEffect(() => {
    setClientReady(true);
  }, []);

  const onFinish = (values) => {
    console.log("Finish:", values);
  };

  const showModal = () => setIsModalOpen(true);
  const handleCancel = () => setIsModalOpen(false);

  return (
    <>
      <Flex vertical gap={10} style={{ width: "15rem", padding: "0 5px" }}>
        <Button
          onClick={showModal}
          style={{ border: "none", padding: "0", justifyContent: "start" }}
        >
          <NumberOutlined style={{ color: "gray" }} />
          <Flex vertical>
            <p style={{ fontWeight: "600", textAlign: "start" }}>Add Project</p>
            <p style={{ fontSize: "12px", color: "gray" }}>
              Plan and assign tasks
            </p>
          </Flex>
        </Button>
        <Button
          style={{ border: "none", padding: "0", justifyContent: "start" }}
        >
          <GroupOutlined style={{ color: "gray" }} />
          <Flex vertical>
            <p style={{ fontWeight: "600", textAlign: "start" }}>
              Browse temlates
            </p>
            <p style={{ fontSize: "12px", color: "gray" }}>
              Get started with a roject template
            </p>
          </Flex>
        </Button>
      </Flex>

      <Modal
        open={isModalOpen}
        onCancel={handleCancel}
        closable={false}
        footer={[]}
        width={"30rem"}
      >
        <Form
          form={form}
          name="horizontal_login"
          layout="inline"
          onFinish={onFinish}
        >
          <Flex vertical style={{ width: "100%" }} gap={12}>
            <p style={{ fontWeight: "600", fontSize: "1.3rem" }}>Add project</p>
            <Divider style={{ margin: "0 0 15px 0" }} />

            <p style={{ fontWeight: "600" }}>Name</p>
            <Form.Item
              name="taskName"
              rules={[{ required: true, message: "Please input task name" }]}
            >
              <Input
                autoFocus
                placeholder=""
                style={{
                  fontSize: "1rem",
                  outline: "transparent",
                  padding: "0 0 0 5px",
                }}
              />
            </Form.Item>
            <Form.Item name="description">
              <p style={{ fontWeight: "600" }}>Color</p>
              {/* <ColorDropDown /> */}
            </Form.Item>
            <Form.Item name="description">
              {/* <p style={{ fontWeight: "600" }}>WorkSpace</p> */}
              {/* <ProjectsDropDown /> */}
            </Form.Item>
            <Form.Item name="description">
              <ToggleFavorite />
            </Form.Item>
          </Flex>
          <Divider style={{ margin: "0 0 15px 0" }} />
          <Flex gap={8} justify="end" style={{ width: "100%" }}>
            <Button key="back" onClick={handleCancel}>
              Cancel
            </Button>
            <Form.Item shouldUpdate>
              {() => (
                <Button
                  type="primary"
                  htmlType="submit"
                  disabled={
                    !clientReady ||
                    !form.isFieldTouched("taskName") ||
                    !form.getFieldValue("taskName")
                  }
                >
                  Add
                </Button>
              )}
            </Form.Item>
          </Flex>
        </Form>
      </Modal>
    </>
  );
};
export default AddOption;
