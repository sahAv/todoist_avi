import React from "react";
import {
  LogoutOutlined,
  PlusOutlined,
  SettingOutlined,
  SmileOutlined,
  FundOutlined,
  BookOutlined,
  StarOutlined,
  SyncOutlined,
  PrinterOutlined,
} from "@ant-design/icons";
import { Button, Divider, Flex, Space } from "antd";

const Profile = ({ handleButtonClick }) => {
  return (
    <Flex vertical style={{ width: "16rem", padding: "0 10px" }}>
      <Button
        key="Profile"
        type="text"
        onClick={() => handleButtonClick("@")}
        style={{
          justifyContent: "start",
          border: "none",
          width: "100%",
          padding: "0",
        }}
      >
        <Space size={12}>
          <SmileOutlined style={{ color: "gray" }} />
          <p style={{ fontSize: "0.9rem", fontWeight: "600" }}>AvishekSah</p>
        </Space>
      </Button>
      <Divider style={{ margin: "5px 0" }} />
      <Button
        key="location"
        type="text"
        style={{
          justifyContent: "start",
          border: "none",
          width: "100%",
          padding: "0",
        }}
      >
        <Space size={12}>
          <SettingOutlined style={{ color: "gray" }} />
          <p style={{ fontSize: "0.8rem" }}>Settings</p>
        </Space>
      </Button>
      <Button
        key="extension"
        type="text"
        style={{
          justifyContent: "start",
          border: "none",
          width: "100%",
          padding: "0",
        }}
      >
        <Space size={12}>
          <PlusOutlined style={{ color: "gray" }} />
          <p style={{ fontSize: "0.8rem" }}>Add a team</p>
        </Space>
      </Button>
      <Divider style={{ margin: "5px 0" }} />

      <Button
        key="team"
        type="text"
        style={{
          justifyContent: "start",
          border: "none",
          width: "100%",
          padding: "0",
        }}
      >
        <Space size={12}>
          <FundOutlined style={{ color: "gray" }} />
          <p style={{ fontSize: "0.8rem" }}>Activity log</p>
        </Space>
      </Button>
      <Button
        key="log"
        type="text"
        style={{
          justifyContent: "start",
          border: "none",
          width: "100%",
          padding: "0",
        }}
      >
        <Space size={12}>
          <PrinterOutlined style={{ color: "gray" }} />
          <p style={{ fontSize: "0.8rem" }}>Print</p>
        </Space>
      </Button>
      <Button
        key="print"
        type="text"
        style={{
          justifyContent: "start",
          border: "none",
          width: "100%",
          padding: "0",
        }}
      >
        <Space size={12}>
          <BookOutlined style={{ color: "gray" }} />
          <p style={{ fontSize: "0.8rem" }}>Resources</p>
        </Space>
      </Button>
      <Divider style={{ margin: "5px 0" }} />

      <Button
        key="resources"
        type="text"
        style={{
          justifyContent: "start",
          border: "none",
          width: "100%",
          padding: "0",
        }}
      >
        <Space size={12}>
          <StarOutlined style={{ color: "gray" }} />
          <p style={{ fontSize: "0.8rem" }}>Upgrade to pro</p>
        </Space>
      </Button>
      <Divider style={{ margin: "5px 0" }} />
      <Button
        key="pro"
        type="text"
        style={{
          justifyContent: "start",
          border: "none",
          width: "100%",
          padding: "0",
        }}
      >
        <Space size={12}>
          <SyncOutlined style={{ color: "gray" }} />
          <p style={{ fontSize: "0.8rem" }}>Sync</p>
        </Space>
      </Button>
      <Divider style={{ margin: "5px 0" }} />
      <Button
        key="sync"
        type="text"
        style={{
          justifyContent: "start",
          border: "none",
          width: "100%",
          padding: "0",
        }}
      >
        <Space size={12}>
          <LogoutOutlined style={{ color: "gray" }} />
          <p style={{ fontSize: "0.8rem" }}>Log out</p>
        </Space>
      </Button>
      <Divider style={{ margin: "5px 0" }} />
      <Button
        key="logout"
        type="text"
        style={{
          justifyContent: "start",
          border: "none",
          width: "100%",
          padding: "0",
        }}
      >
        <p style={{ fontSize: "0.8rem", color: "green", fontWeight: "600" }}>
          New Version Available
        </p>
      </Button>
      <Flex gap={15}>
        <Button
          key="code"
          type="text"
          style={{
            justifyContent: "start",
            border: "none",
            //   width: "100%",
            padding: "0",
          }}
        >
          <p style={{ fontSize: "0.8rem" }}>V6154</p>
        </Button>
        <Button
          key="new"
          type="text"
          style={{
            justifyContent: "start",
            border: "none",
            //   width: "100%",
            padding: "0",
          }}
        >
          <p style={{ fontSize: "0.8rem" }}>What's new</p>
        </Button>
      </Flex>
    </Flex>
  );
};
export default Profile;
