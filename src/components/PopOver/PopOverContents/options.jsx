import React from "react";
import { EditOutlined } from "@ant-design/icons";
import { Flex } from "antd";

const Options = () => {
  return (
    <Flex vertical gap={7} style={{ width: "12rem", padding: "0 5px" }}>
      <Flex gap={15}>
        <EditOutlined />
        <p>Edit</p>
      </Flex>
      <Flex gap={15}>
        <EditOutlined />
        <p>Add to favourite</p>
      </Flex>
      <Flex gap={15}>
        <EditOutlined />
        <p>Delete</p>
      </Flex>
    </Flex>
  );
};
export default Options;
