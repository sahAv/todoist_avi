import React from "react";
import { FlagFilled, FlagOutlined } from "@ant-design/icons";
import { List } from "antd";

const Priority = () => {
  return (
    <div>
      <List>
        <List.Item style={{ padding: "5px 0" }}>
          <FlagFilled style={{ marginRight: "10px", color: "red" }} />
          Priority 1
        </List.Item>
        <List.Item style={{ padding: "5px 0" }}>
          <FlagFilled style={{ marginRight: "10px", color: "orange" }} />
          Priority 2
        </List.Item>
        <List.Item style={{ padding: "5px 0" }}>
          <FlagFilled style={{ marginRight: "10px", color: "blue" }} />
          Priority 3
        </List.Item>
        <List.Item style={{ padding: "5px  0" }}>
          <FlagOutlined style={{ marginRight: "10px", color: "black" }} />
          Priority 4
        </List.Item>
      </List>
    </div>
  );
};
export default Priority;
