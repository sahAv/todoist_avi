import React from "react";
import {
  SelectOutlined,
  EnvironmentOutlined,
  TagOutlined,
} from "@ant-design/icons";
import { Button, Divider, Flex, Space } from "antd";

const Label = ({ handleButtonClick }) => {
  return (
    <Flex vertical style={{ width: "10rem" }}>
      <Button
        key="label"
        type="text"
        onClick={() => handleButtonClick("@")}
        style={{
          justifyContent: "start",
          border: "none",
          width: "100%",
          padding: "0",
        }}
      >
        <TagOutlined style={{ color: "gray" }} />
        <Space size={80}>
          <p style={{ fontSize: "0.8rem" }}>Labels</p>
          <p style={{ color: "gray" }}>@</p>
        </Space>
      </Button>
      <Button
        key="location"
        type="text"
        style={{
          justifyContent: "start",
          border: "none",
          width: "100%",
          padding: "0",
        }}
      >
        <EnvironmentOutlined style={{ color: "gray" }} />
        <p style={{ fontSize: "0.8rem" }}>Location</p>
      </Button>
      <Divider style={{ margin: "5px 0" }} />
      <Button
        key="extension"
        type="text"
        style={{
          justifyContent: "start",
          border: "none",
          width: "100%",
          padding: "0",
        }}
      >
        <SelectOutlined style={{ color: "gray" }} />
        <p style={{ fontSize: "0.8rem" }}>Add extension...</p>
      </Button>
      <Divider style={{ margin: "5px 0" }} />
      <Button
        key="tasks"
        type="text"
        style={{
          justifyContent: "start",
          border: "none",
          width: "100%",
          padding: "0",
        }}
      >
        <p style={{ fontSize: "0.8rem", color: "red" }}>Edit task actions</p>
      </Button>
    </Flex>
  );
};
export default Label;
