import { InboxOutlined, UserOutlined } from "@ant-design/icons";
import { Flex, Button } from "antd";
import Input from "antd/es/input/Input";

const Inbox = ({ project, onButtonClick }) => {
  const handleButtonClick = (text) => {
    onButtonClick(text);
  };
  return (
    <Flex vertical gap={5}>
      <Input autoFocus placeholder="Type a project name" />
      <Flex vertical>
        <Button
          key="inbox"
          type="text"
          onClick={() => handleButtonClick("Inbox1")}
          style={{
            justifyContent: "start",
            border: "none",
            width: "100%",
            padding: "0",
          }}
        >
          <InboxOutlined />
          <p style={{ fontSize: "0.8rem" }}>Inbox</p>
        </Button>
        <Flex gap={8}>
          <UserOutlined />
          <p
            style={{
              fontWeight: "500",
              fontSize: "0.85rem",
              height: "1.9rem",
              alignContent: "center",
            }}
          >
            My projects
          </p>
        </Flex>
        {project.map((project) => (
          <Button
            key={project.name}
            type="text"
            onClick={() => handleButtonClick(project.name)}
            style={{
              justifyContent: "start",
              border: "none",
              fontSize: "0.8rem",
            }}
          >
            <p
              style={{
                color: project.color,
                fontSize: "1.2rem",
                fontWeight: "300",
              }}
            >
              #
            </p>
            {project.name}
          </Button>
        ))}
      </Flex>
    </Flex>
  );
};
export default Inbox;
