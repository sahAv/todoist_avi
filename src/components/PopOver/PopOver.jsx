import React from "react";
import { Button, Popover, Space, Tooltip } from "antd";

const PopOver = ({
  title,
  icon,
  border,
  content,
  placement,
  fontSize,
  fontWeight,
  padding,
  icon2,
  background,
  tooltip,
}) => {
  return (
    <Space wrap>
      <Popover
        content={content}
        trigger="click"
        placement={placement ? placement : "bottom"}
        arrow={false}
      >
        {title === "AvishekSah" ? (
          <Tooltip
            title={
              <span style={{ fontSize: "14px", lineHeight: "2px" }}>
                {tooltip}
              </span>
            }
            placement="right"
            arrow={false}
          >
            <Button
              type="text"
              onClick={(e) => {
                e.stopPropagation();
              }}
              style={{
                border: border,
                background: background,
                padding: padding,
                fontWeight: fontWeight,
                fontSize: fontSize,
              }}
            >
              {title ? (
                icon
              ) : (
                <Tooltip
                  title={
                    <span style={{ fontSize: "14px", lineHeight: "2px" }}>
                      {tooltip}
                    </span>
                  }
                  placement="right"
                  arrow={false}
                >
                  {icon}
                </Tooltip>
              )}
              {title}
              {icon2}
            </Button>
          </Tooltip>
        ) : (
          <Button
            type="text"
            onClick={(e) => {
              e.stopPropagation();
            }}
            style={{
              border: border,
              background: background,
              padding: padding,
              fontWeight: fontWeight,
              fontSize: fontSize,
            }}
          >
            {title ? (
              icon
            ) : (
              <Tooltip
                title={
                  <span style={{ fontSize: "12px", lineHeight: "2px" }}>
                    {tooltip}
                  </span>
                }
                placement="right"
                arrow={false}
              >
                {icon}
              </Tooltip>
            )}
            {title}
            {icon2}
          </Button>
        )}
      </Popover>
    </Space>
  );
};
export default PopOver;

// import React from "react";
// import { Button, Popover, Space, Tooltip } from "antd";

// const PopOver = ({
//   title,
//   icon,
//   border,
//   content,
//   placement,
//   fontSize,
//   fontWeight,
//   padding,
//   icon2,
//   background,
//   tooltip,
// }) => {
//   return (
//     <Space wrap>
//       <Popover
//         content={content}
//         trigger="click"
//         placement={placement ? placement : "bottom"}
//         arrow={false}
//       >
//         {title === "AvishekSah" ? (
//           <Tooltip
//             title={
//               <span style={{ fontSize: "12px", lineHeight: "2px" }}>
//                 {tooltip}
//               </span>
//             }
//             placement="right"
//             arrow={false}
//           >
//             <Button
//               type="text"
//               onClick={(e) => {
//                 e.stopPropagation();
//               }}
//               style={{
//                 border: border,
//                 background: background,
//                 padding: padding,
//                 fontWeight: fontWeight,
//                 fontSize: fontSize,
//               }}
//             >
//               {icon}
//               {title}
//               {icon2}
//             </Button>
//           </Tooltip>
//         ) : (
//           <Button
//             type="text"
//             onClick={(e) => {
//               e.stopPropagation();
//             }}
//             style={{
//               border: border,
//               background: background,
//               padding: padding,
//               fontWeight: fontWeight,
//               fontSize: fontSize,
//             }}
//           >
//             {icon}
//             {title}
//             {icon2}
//           </Button>
//         )}
//       </Popover>
//     </Space>
//   );
// };
// export default PopOver;
