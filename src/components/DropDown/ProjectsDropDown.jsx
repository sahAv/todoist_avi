// import React from "react";
// import { UserOutlined, DownOutlined, PlusOutlined } from "@ant-design/icons";
// import { Dropdown, Space } from "antd";

// const items = [
//   {
//     label: "My projets",
//     key: "0",
//     icon: <UserOutlined />,
//   },
//   {
//     label: "Add new Projects",
//     key: "1",
//     icon: <PlusOutlined />,
//   },
// ];
// const ProjectsDropDown = () => {
//   return (
//     <Dropdown
//       menu={{
//         items,
//       }}
//       trigger={["click"]}
//     >
//       <a onClick={(e) => e.preventDefault()}>
//         <Space>
//           Color
//           <DownOutlined />
//         </Space>
//       </a>
//     </Dropdown>
//   );
// };
// export default ProjectsDropDown;

import React, { useState } from "react";
import { DownOutlined, UserOutlined, PlusOutlined } from "@ant-design/icons";
import { Button, Dropdown, Menu } from "antd";

const ProjectsDropDown = () => {
  const items = [
    {
      label: "My projets",
      key: "My projets",
      icon: <UserOutlined />,
    },
    {
      label: "Add new Projects",
      key: "Add new Projects",
      icon: <PlusOutlined />,
    },
  ];

  const [selectedLabel, setSelectedLabel] = useState(items[0].label);

  const handleMenuClick = (e) => {
    setSelectedLabel(e.key);
  };

  const menu = (
    <Menu onClick={handleMenuClick}>
      {items.map((item) => (
        <Menu.Item key={item.key} icon={item.icon}>
          {item.label}
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <Dropdown overlay={menu}>
      <Button
        style={{
          display: "flex",
          justifyContent: "space-between",
          width: "100%",
        }}
      >
        {selectedLabel} <DownOutlined />
      </Button>
    </Dropdown>
  );
};

export default ProjectsDropDown;
