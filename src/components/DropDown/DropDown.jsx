import React from "react";
import { DownOutlined, BellOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Dropdown, Flex, Space, Tooltip, Typography } from "antd";

const { Text } = Typography;
const items = [
  {
    label: <a>1st menu item</a>,
    key: "0",
  },
  {
    label: <a>2nd menu item</a>,
    key: "1",
  },
  {
    type: "divider",
  },
  {
    label: "3rd menu item",
    key: "3",
  },
];
const DropDown = () => {
  return (
    <>
      <Flex justify="space-between" align="center" style={{ width: "85%" }}>
        <Dropdown
          menu={{
            items,
          }}
          trigger={["click"]}
        >
          <a onClick={(e) => e.preventDefault()} style={{ color: "black" }}>
            <Tooltip
              title={
                <span style={{ fontSize: "12px", lineHeight: "2px" }}>
                  Daily goal: 0/5 tasks
                </span>
              }
              placement="right"
              arrow={false}
            >
              <Button
                type="text"
                style={{
                  border: "none",
                  otline: "none",
                  padding: "0",
                }}
              >
                <Flex
                  gap={10}
                  style={{ padding: "5px 0", borderRadius: "5px" }}
                >
                  <i
                    style={{ fontSize: "1.6rem", background: "transparent" }}
                    className="fa-solid fa-circle-user"
                  ></i>
                  {/* <img src="src/Img/UserIcon.png" width={20}/> */}
                  <Space>
                    <Text
                      level={5}
                      style={{
                        fontWeight: "600",
                      }}
                    >
                      AvishekSah
                    </Text>
                    <DownOutlined style={{ width: "10px" }} />
                  </Space>
                </Flex>
              </Button>
            </Tooltip>
          </a>
        </Dropdown>
        <Tooltip
          title={
            <span style={{ fontSize: "12px", lineHeight: "2px" }}>
              Open notifications
            </span>
          }
          placement="bottom"
          arrow={false}
        >
          <Button
            size="small"
            type="text"
            style={{
              border: "none",
              otline: "none",
            }}
          >
            <BellOutlined style={{ fontSize: "1.1rem", color: "gray" }} />
          </Button>
        </Tooltip>
      </Flex>
    </>
  );
};
export default DropDown;
