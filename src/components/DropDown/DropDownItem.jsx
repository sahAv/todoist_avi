import React from "react";
import { Collapse, Flex, Tooltip } from "antd";
import PopOver from "../PopOver/PopOver";
import Options from "../PopOver/PopOverContents/options";
import AddOption from "../PopOver/PopOverContents/AddOption";
import { EllipsisOutlined, PlusOutlined } from "@ant-design/icons";

const DropDownItem = ({ title, project }) => {
  return (
    <Collapse
      collapsible="icon"
      defaultActiveKey={["0"]}
      style={{
        outline: "none",
        border: "none",
        background: "none",
        fontWeight: "600",
      }}
      expandIconPosition="end"
      items={[
        {
          label: (
            <Flex
              justify="space-between"
              align="start"
              style={{ cursor: "pointer" }}
            >
              <p>{title}</p>
              {title === "My Projects" && (
                <PopOver
                  icon={<PlusOutlined />}
                  border={"none"}
                  content={<AddOption />}
                  placement={"right"}
                />
              )}
            </Flex>
          ),
          children: (
            <Flex vertical gap={8}>
              {project?.map((item) => (
                <Tooltip
                  key={item.id}
                  title={item.name}
                  placement="right"
                  arrow={false}
                >
                  <Flex justify="space-between" style={{ cursor: "pointer" }}>
                    <Flex align="center" gap={10}>
                      <p
                        style={{
                          color: item.color,
                          fontSize: "1.2rem",
                          fontWeight: "300",
                        }}
                      >
                        #
                      </p>
                      <p>{item.name}</p>
                    </Flex>
                    <PopOver
                      icon={<EllipsisOutlined />}
                      border={"none"}
                      content={<Options />}
                      placement={"right"}
                    />
                  </Flex>
                </Tooltip>
              ))}
            </Flex>
          ),
        },
      ]}
    />
  );
};
export default DropDownItem;
