import React, { useState } from "react";
import { DownOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Dropdown, Menu, ColorPicker } from "antd";

const ColorDropDown = () => {
  const items = [
    {
      label: "Red",
      key: "Red",
      icon: <ColorPicker defaultValue="red" style={{ border: "none" }} />,
    },
    {
      label: "Yellow",
      key: "Yellow",
      icon: <ColorPicker defaultValue="yellow" style={{ border: "none" }} />,
    },
    {
      label: "Blue",
      key: "Blue",
      icon: <ColorPicker defaultValue="blue" style={{ border: "none" }} />,
    },
    {
      label: "Green",
      key: "Green",
      icon: <ColorPicker defaultValue="green" style={{ border: "none" }} />,
    },
  ];

  const [selectedLabel, setSelectedLabel] = useState(items[0].label);

  const handleMenuClick = (e) => {
    setSelectedLabel(e.key);
  };

  const menu = (
    <Menu onClick={handleMenuClick}>
      {items.map((item) => (
        <Menu.Item key={item.key} icon={item.icon}>
          {item.label}
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    // <Dropdown overlay={menu}>
      <Button
        style={{
          display: "flex",
          justifyContent: "space-between",
          width: "100%",
        }}
      >
        {selectedLabel} <DownOutlined />
      </Button>
    // </Dropdown>
  );
};

export default ColorDropDown;
