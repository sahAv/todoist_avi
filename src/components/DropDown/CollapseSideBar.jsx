import { Button, Layout, Tooltip } from "antd";
import React, { useState } from "react";
import SideBar from "../SideBar/SideBar";

const { Sider, Content } = Layout;

const CollapseSideBar = ({ project }) => {
  const [collapsed, setCollapsed] = useState(false);
  const [content, setContent] = useState("");

  const toggleSidebar = () => {
    setCollapsed(!collapsed);
  };

  const handleTabChange = (title) => {
    setContent(title);
  };
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider
        collapsed={collapsed}
        onCollapse={toggleSidebar}
        collapsedWidth={0}
        width="fit-content"
      >
        <SideBar project={project} onTabChange={handleTabChange} />
      </Sider>
      <Content>
        <div
          style={{ padding: "2rem 5rem", background: "#fff", height: "100vh" }}
        >
          {content}
        </div>
      </Content>
      <Tooltip
        title={
          <span style={{ fontSize: "12px", lineHeight: "2px" }}>
            Open/close sidebar
          </span>
        }
        placement="bottom"
        arrow={false}
      >
        <Button
          size="small"
          type="text"
          style={{
            border: "none",
            otline: "none",
            position: "fixed",
            top: 15,
            left: collapsed ? 10 : 238,
          }}
        >
          <svg
            onClick={toggleSidebar}
            cursor="pointer"
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            className="bi bi-layout-sidebar"
            viewBox="0 0 16 16"
          >
            <path d="M0 3a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2zm5-1v12h9a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1zM4 2H2a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h2z" />
          </svg>
        </Button>
      </Tooltip>
    </Layout>
  );
};

export default CollapseSideBar;
