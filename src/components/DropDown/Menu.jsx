import React from "react";
import { Flex, Menu, Tooltip } from "antd";
import { EllipsisOutlined } from "@ant-design/icons";
import PopOver from "../PopOver/PopOver";
import Options from "../PopOver/PopOverContents/options";
import AddOption from "../PopOver/PopOverContents/AddOption";

const MenuBar = ({ title, project, activeItem, setActiveItem }) => {
  const items = [
    {
      key: "sub1",
      label: (
        <Flex
          justify="space-between"
          style={{ width: "82%" }}
          className="flex-container"
        >
          {title}
          {title === "My Projects" && (
            <PopOver
              tooltip={"My projects menu"}
              icon={"+"}
              border={"none"}
              content={<AddOption />}
              placement={"right"}
              fontSize={"23px"}
              fontWeight={"100"}
              padding={"0"}
              background={"none"}
            />
          )}
        </Flex>
      ),
      children: project.map((item) => ({
        key: item.name,
        icon: (
          <p
            style={{
              color: item.color,
              fontSize: "1.2rem",
              fontWeight: "300",
            }}
          >
            #
          </p>
        ),
        label: (
          <Tooltip
            title={
              <span style={{ fontSize: "12px", lineHeight: "2px" }}>
                {item.name}
              </span>
            }
            placement="right"
            arrow={false}
          >
            <Flex justify="space-between">
              {item.name}
              <PopOver
                tooltip={"More project actions"}
                icon={<EllipsisOutlined />}
                border={"none"}
                content={<Options />}
                placement={"right"}
                background={"none"}
              />
            </Flex>
          </Tooltip>
        ),
      })),
    },
  ];

  const onClick = (e) => {
    setActiveItem(e.key);
  };

  return (
    <Menu
      onClick={onClick}
      style={{
        width: 256,
        background: "transparent",
        fontWeight: "600",
      }}
      defaultSelectedKeys={["1"]}
      defaultOpenKeys={["sub1"]}
      mode="inline"
      className="custom-menu"
      items={items}
      selectedKeys={[activeItem]}
    />
  );
};

export default MenuBar;
