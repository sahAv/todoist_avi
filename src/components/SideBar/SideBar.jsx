import { React, useState } from "react";
import { Button, Flex, Tooltip } from "antd";
import MenuBar from "../DropDown/Menu";
import {
  PlusCircleFilled,
  SearchOutlined,
  InboxOutlined,
  CalendarOutlined,
  CalendarFilled,
  AppstoreOutlined,
  AppstoreFilled,
  PlusOutlined,
  BellOutlined,
} from "@ant-design/icons";
import Profile from "../PopOver/PopOverContents/Profile";
import PopOver from "../PopOver/PopOver";
import SampleModal from "../Modal/SampleModal";

const SideBar = ({ project, onTabChange }) => {
  const [activeButton, setActiveButton] = useState("Inbox");

  const handleTabChange = (title) => {
    setActiveButton(title);
    onTabChange(title);
  };
  return (
    <>
      <Flex
        vertical
        style={{
          backgroundColor: "#fcfaf8",
          width: "17.4rem",
          height: "100vh",
          justifyContent: "space-between",
          padding: "11px 15px",
        }}
      >
        <Flex vertical gap={12}>
          <Flex align="center" justify="space-between" style={{ width: "87%" }}>
            <PopOver
              title={"AvishekSah"}
              tooltip={"Daily goal: 0/5 tasks"}
              fontWeight={"600"}
              icon={
                <i
                  style={{ fontSize: "1.6rem", background: "transparent" }}
                  className="fa-solid fa-circle-user"
                ></i>
              }
              icon2={
                <i
                  className="fa-solid fa-chevron-down"
                  style={{ fontSize: "0.5rem" }}
                />
              }
              border="none"
              padding={"0"}
              content={<Profile />}
            />
            <Tooltip
              title={
                <span style={{ fontSize: "12px", lineHeight: "2px" }}>
                  Open notifications
                </span>
              }
              placement="bottom"
              arrow={false}
            >
              <Button
                size="small"
                type="text"
                style={{
                  border: "none",
                  otline: "none",
                }}
              >
                <BellOutlined style={{ fontSize: "1.1rem", color: "gray" }} />
              </Button>
            </Tooltip>
          </Flex>
          <Flex vertical gap={6} style={{ paddingLeft: "1px" }}>
            <SampleModal
              projects={project}
              title={"Add Task"}
              fontColor={"#a81f00"}
              fontweight={"600"}
              icon={
                <PlusCircleFilled
                  style={{ fontSize: "22px", color: "#dc4c3e" }}
                />
              }
            />
            <Flex vertical gap={3} style={{ padding: "0 3px" }}>
              <Tooltip
                title={
                  <span style={{ fontSize: "12px", lineHeight: "2px" }}>
                    Open quick find
                  </span>
                }
                placement="right"
                arrow={false}
              >
                <Button
                  type="text"
                  style={{
                    display: "flex",
                    justifyContent: "start",
                    padding: "0",
                  }}
                >
                  <SearchOutlined style={{ fontSize: "19px", color: "gray" }} />
                  Search
                </Button>
              </Tooltip>
              <Tooltip
                title={
                  <span style={{ fontSize: "12px", lineHeight: "2px" }}>
                    Go to Inbox
                  </span>
                }
                placement="right"
                arrow={false}
              >
                <Button
                  type="text"
                  onClick={() => handleTabChange("Inbox")}
                  style={{
                    display: "flex",
                    justifyContent: "start",
                    padding: "0",
                    color: activeButton === "Inbox" ? "#a81f00" : "black",
                    background:
                      activeButton === "Inbox" ? "#fdeeea" : undefined,
                  }}
                >
                  <InboxOutlined
                    style={{
                      fontSize: "19px",
                      color: activeButton === "Inbox" ? "#dc4c3e" : "gray",
                    }}
                  />
                  Inbox
                </Button>
              </Tooltip>
              <Tooltip
                title={
                  <span style={{ fontSize: "12px", lineHeight: "2px" }}>
                    Go to Today
                  </span>
                }
                placement="right"
                arrow={false}
              >
                <Button
                  type="text"
                  onClick={() => handleTabChange("Today")}
                  style={{
                    display: "flex",
                    justifyContent: "start",
                    padding: "0",
                    color: activeButton === "Today" ? "#a81f00" : "black",
                    background:
                      activeButton === "Today" ? "#fdeeea" : undefined,
                  }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="currentColor"
                    className="bi bi-calendar-date"
                    viewBox="0 0 16 16"
                    style={{
                      marginLeft: "1px",
                      color: activeButton === "Today" ? "#a81f00" : "gray",
                    }}
                  >
                    <path d="M6.445 11.688V6.354h-.633A13 13 0 0 0 4.5 7.16v.695c.375-.257.969-.62 1.258-.777h.012v4.61zm1.188-1.305c.047.64.594 1.406 1.703 1.406 1.258 0 2-1.066 2-2.871 0-1.934-.781-2.668-1.953-2.668-.926 0-1.797.672-1.797 1.809 0 1.16.824 1.77 1.676 1.77.746 0 1.23-.376 1.383-.79h.027c-.004 1.316-.461 2.164-1.305 2.164-.664 0-1.008-.45-1.05-.82zm2.953-2.317c0 .696-.559 1.18-1.184 1.18-.601 0-1.144-.383-1.144-1.2 0-.823.582-1.21 1.168-1.21.633 0 1.16.398 1.16 1.23" />
                    <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z" />
                  </svg>
                  Today
                </Button>
              </Tooltip>
              <Tooltip
                title={
                  <span style={{ fontSize: "12px", lineHeight: "2px" }}>
                    Go to Upcoming
                  </span>
                }
                placement="right"
                arrow={false}
              >
                <Button
                  type="text"
                  onClick={() => handleTabChange("Upcoming")}
                  style={{
                    display: "flex",
                    justifyContent: "start",
                    padding: "0",
                    color: activeButton === "Upcoming" ? "#a81f00" : "black",
                    background:
                      activeButton === "Upcoming" ? "#fdeeea" : undefined,
                  }}
                >
                  {activeButton === "Upcoming" ? (
                    <CalendarFilled
                      style={{
                        fontSize: "19px",
                        color: activeButton === "Upcoming" ? "#dc4c3e" : "gray",
                      }}
                    />
                  ) : (
                    <CalendarOutlined
                      style={{
                        fontSize: "19px",
                        color: activeButton === "Upcoming" ? "#a81f00" : "gray",
                      }}
                    />
                  )}
                  Upcoming
                </Button>
              </Tooltip>
              <Tooltip
                title={
                  <span style={{ fontSize: "12px", lineHeight: "2px" }}>
                    Go to Filters & Labels
                  </span>
                }
                placement="right"
                arrow={false}
              >
                <Button
                  type="text"
                  onClick={() => handleTabChange("Filters & Labels")}
                  style={{
                    display: "flex",
                    justifyContent: "start",
                    padding: "0",
                    color:
                      activeButton === "Filters & Labels" ? "#a81f00" : "black",
                    background:
                      activeButton === "Filters & Labels"
                        ? "#fdeeea"
                        : undefined,
                  }}
                >
                  {activeButton === "Filters & Labels" ? (
                    <AppstoreFilled
                      style={{
                        fontSize: "19px",
                        color:
                          activeButton === "Filters & Labels"
                            ? "#dc4c3e"
                            : "gray",
                      }}
                    />
                  ) : (
                    <AppstoreOutlined
                      style={{
                        fontSize: "19px",
                        color:
                          activeButton === "Filters & Labels"
                            ? "#a81f00"
                            : "gray",
                      }}
                    />
                  )}
                  Filters & Labels
                </Button>
              </Tooltip>
            </Flex>
          </Flex>
          <Flex vertical>
            <MenuBar
              title={"Favorites"}
              project={project.filter((item) => item.isFavorite === true)}
              activeItem={activeButton}
              setActiveItem={setActiveButton}
            />
            <MenuBar
              title={"My Projects"}
              project={project}
              activeItem={activeButton}
              setActiveItem={setActiveButton}
            />
          </Flex>
        </Flex>
        <Flex>
          <SampleModal
            projects={[]}
            title={"Add a team"}
            fontweight={"600"}
            width={"100%"}
            icon={
              <PlusOutlined style={{ fontSize: "20px", fontweight: "300" }} />
            }
          />
        </Flex>
      </Flex>
    </>
  );
};
export default SideBar;
