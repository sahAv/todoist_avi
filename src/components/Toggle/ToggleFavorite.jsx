import React from "react";
import { Flex, Switch } from "antd";
const onChange = (checked) => {
  console.log(`switch to ${checked}`);
};
const ToggleFavorite = () => {
  return (
    <Flex gap={10}>
      <Switch defaultValue={false} onChange={onChange} />
      <p>Add to favorites</p>
    </Flex>
  );
};
export default ToggleFavorite;
