import React, { useState } from "react";
import { Button, Flex, Input, Modal } from "antd";

const ModalDate = ({ title, icon, fontColor, fontweight, border }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <>
      <Button
        size="small"
        style={{
          justifyContent: "start",
          color: fontColor,
          fontWeight: fontweight,
          border: border,
          float: "left",
        }}
        type="secondary"
        onClick={showModal}
      >
        {icon}
        {title}
      </Button>
      <Modal
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        closable={false}
        width={260}
        footer={null}
      >
        <Input
          placeholder="Task name"
          style={{
            fontWeight: "600",
            fontSize: "1.3rem",
            border: "transparent",
            outline: "transparent",
          }}
        />
        <Input
          placeholder="Description"
          style={{
            fontSize: "0.8rem",
            border: "transparent",
            outline: "transparent",
          }}
        />
      </Modal>
    </>
  );
};
export default ModalDate;
