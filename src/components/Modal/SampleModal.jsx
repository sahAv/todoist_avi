import React, { useState, useEffect } from "react";
import { Button, Divider, Flex, Input, Modal, Tooltip, Form } from "antd";
import {
  CalendarOutlined,
  FlagOutlined,
  FieldTimeOutlined,
  EllipsisOutlined,
} from "@ant-design/icons";

import PopOver from "../PopOver/PopOver";
import Date from "../PopOver/PopOverContents/Date";
import Priority from "../PopOver/PopOverContents/Priority";
import Inbox from "../PopOver/PopOverContents/Inbox";
import Label from "../PopOver/PopOverContents/Label";

const ModalBox = ({ title, icon, fontColor, fontweight, width, projects }) => {
  const [form] = Form.useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [clientReady, setClientReady] = useState(false);
  const [popoverTitle, setPopoverTitle] = useState("");
  const [atText, setAtText] = useState("");


  useEffect(() => {
    setClientReady(true);
  }, []);

  const onFinish = (values) => {
    console.log("Finish:", values);
  };

  const showModal = () => setIsModalOpen(true);
  const handleCancel = () => setIsModalOpen(false);

  return (
    <>
      <Tooltip
        title={
          <span style={{ fontSize: "12px", lineHeight: "2px" }}>
            {`${title}`}
          </span>
        }
        placement="right"
        arrow={false}
      >
        <Button
          style={{
            width: width,
            justifyContent: "start",
            color: fontColor,
            fontWeight: fontweight,
            padding: "0",
          }}
          type="text"
          onClick={showModal}
        >
          {icon}
          {title}
        </Button>
      </Tooltip>
      <Modal
        open={isModalOpen}
        onCancel={handleCancel}
        closable={false}
        footer={[]}
      >
        <Form form={form} layout="inline" onFinish={onFinish}>
          <Flex vertical style={{ width: "100%" }}>
            <Form.Item
              name="taskName"
              // rules={[{ required: true, message: "Please input task name" }]}
            >
              <Input
                placeholder="Task name"
                
                style={{
                  fontWeight: "600",
                  fontSize: "1.3rem",
                  border: "transparent",
                  outline: "transparent",
                  padding: "0",
                }}
              />
            </Form.Item>
            <Form.Item name="description">
              <Input
                placeholder="Description"
                style={{
                  fontSize: "0.8rem",
                  border: "transparent",
                  outline: "transparent",
                  padding: "0",
                }}
              />
            </Form.Item>
          </Flex>
          <Flex gap={8} style={{ margin: "12px 0" }}>
            <PopOver
              title="Due Date"
              icon={<CalendarOutlined />}
              border="1px solid #d9d9d9"
              content={<Date />}
            />
            <PopOver
              title="Priority"
              icon={<FlagOutlined />}
              border="1px solid #d9d9d9"
              content={<Priority />}
            />
            <PopOver
              title="Reminders"
              icon={<FieldTimeOutlined />}
              border="1px solid #d9d9d9"
            />
            <PopOver
              icon={<EllipsisOutlined />}
              border="1px solid #d9d9d9"
              content={<Label handleButtonClick={setAtText} />}
            />
          </Flex>
          <Divider style={{ margin: "0 0 15px 0" }} />
          <Flex justify="space-between" style={{ width: "100%" }}>
            <Flex>
              <PopOver
                title={popoverTitle || "Inbox"}
                icon={<CalendarOutlined />}
                border="none"
                padding={"0 4px"}
                content={
                  <Inbox project={projects} onButtonClick={setPopoverTitle} />
                }
              />
            </Flex>
            <Flex gap={8}>
              <Button key="back" onClick={handleCancel}>
                Cancel
              </Button>
              <Form.Item shouldUpdate>
                {() => (
                  <Button
                    type="primary"
                    htmlType="submit"
                    disabled={
                      !clientReady ||
                      !form.isFieldTouched("taskName") ||
                      !form.getFieldValue("taskName")
                    }
                  >
                    Add task
                  </Button>
                )}
              </Form.Item>
            </Flex>
          </Flex>
        </Form>
      </Modal>
    </>
  );
};

export default ModalBox;
