import { useEffect, useState } from "react";
import CollapseSideBar from "./components/DropDown/CollapseSideBar";
import { LoadingOutlined } from "@ant-design/icons";
import GetUserProjects from "./Api/Api";
import { Flex, Image } from "antd";
import { Spin } from "antd";

import "./App.css";

function App() {
  const [project, setProject] = useState([]);
  const [isLoading, setIsloading] = useState(false);

  useEffect(() => {
    setIsloading(true);
    GetUserProjects().then((data) => {
      setProject(data.slice(1));
      setIsloading(false);
    });
  }, []);
  return (
    <>
      {isLoading && (
        <Flex
          vertical
          gap={20}
          justify="center"
          align="center"
          style={{ height: "100vh" }}
        >
          <Image
            width="4rem"
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAkFBMVEXkQzL////kQTDkPy3jOibjPCnjNR/iKAniLRPjNB7jOCTjMhviKg3iLBHlT0D42Nb1w7/76efrfHP98/Luk4zoZVnzubX65OLysKvqdWvmWEvmU0XpbGH2y8jvn5nvm5Tsh3/ysq3lSDjwpqD+9/bnXE/1x8Prgnn87u353dvvnJbqenHshn7tjYbnYVXlTDx9tFpkAAAJqklEQVR4nO2dbXeiPBCGIeFNUNStYqWtrtrWdvu0/v9/9+BLlUAyE5UQyOH6uGf3MMOSzMydmWjZPMajxXYar63WEE+3i9GY64tV/qPhhAw8h1Ki2+wrIIQ6XkAnQwkPk13gtMm3PMQJdgniYeL22ureEdJzE8DDWRq12789JEpnIg83PtVtXiVQf8P3cB7pNq0yojnPwzTUbVeFhGnZw9jVbVWluHHRw9QsBzMXU9bDuUmf6JFwnvdwY84mcyHaXDyc+bqtUYI/O3uYmhEHi9D018PExG90T5ScPHTbn6rxIe7Rw6Sn2xJl9JKDh7GZq3AP2e09HAa67VBIMMw8nDi6zVCIM8k8tEzdZ/YQalvjgW4rlBKMrXdPtxFK8UbWwuRlmC3EhbU1N1bsoVtrariHU+vJ5K0020xjq0XS/U2Y7l9HR0dHR0dHR0dHR1a8uY6xevsed/D2vfy7jkwtwWl/fmwKGT4Z0OJShvTiS4PWyjPvBNqJVvmmnteJIZ08v1D/+9VmGT8H5nyqJJjO7DIva0OEW+KRd45/h34QIxpe3P5G4F+2Gg045qP9Pw9CB207bftSJFHKaeHNEbfcwzAste+yjNv9lVJ/Avtnv761OSaS4JnfSH9hE7TYQeKtXxD/XtZeixehG4gjxGkFtjqjof62mKIVaXVWSqK3T8S/pNWVReiOEP/aXR26/hLx7+FPmz9QOph/IA5uBi1Otpkins+71eYIUSjiOcymdUUIFQudU8QX+a5vAQ4rzyZI8MMr4vOsejVGiH1GWOV6B4r4X4ZxrREie+LrtrpPBirij3zM+/VGiMNTPysaPESK+D2Lft0R4vTgKnInEj1hEWJE60/Rzg+/O/8NPaSIt2c/OlK03PPvilASRfxfPSla3oZ3cmvkkCjiHyNNki9rxm37gEwRv9M2JF6w5Ja93EGL+I/5QF8KWrJmeOXblijil7VHiDwcgx4D+RUjU8Q7eot4nk2v/8nueqGDFfFAMkHc0AvVb68Cu95kIhdexEMJYRhsH5PNs/Iv+PZvS6aIFyf15/g5Vr0Lic2DkxyJIh6QeckgFz8VRxLAQkioxYt44F+X4qfSbAD+XxBIKRJFPPAFOMFj8W+/Kqz5EUN5K0lwEp8HKFWo/x/v7cymqkbMsHD98KeQ5BDPQot4IEKI4+dfRV+qE5U+maK9jCztDrAUrfROcoAiuKqhedLbYZviKgpPPlIfLeIBmdf1/0H/Ut0RKe3PEbNfJ/0odELPn2IpGlCAZY+B4+dOYcxw/QViuf2+mSwSLMIDRTQePz/VnuPjGiAOtOVLiOBrxQVWFgKwKh1h1RNuhnj8zN6O+goSV1ogAJlXIn7WJXGgLS9CAGlAIn7WKHGQKMU2Sy6AvIPHz4/5oE4NTkKTKDFyxSkaLoLXL3HgSQ4LIPOinWzg21FHluRg2uAFSObFl7WclKAAglfvJ4A9UEIEl5aDFIBkkCde1sI9UEYEv0LSUwHeCvPxLBRYGtXJJhxZIRF8WL30hXsgp4gvUGcn21icLEO5FiDFCYr4PLV2soEFjyhfBvZAGRE8rDVC7B8pTkm4NQ8o8+KdbHVf5Xt4KpBWlutWQOZ1+9gWDEkcivh9s7Fw42fNBvZAtIivurNFjvPDgeAdnm+Q/gRk3qZ2sl2eDyRgxIu2j6Nk+SRuN29SJxtL3gZoi3RDz3OE/kkV8ZpSNNaO0U2nmRJFfK2dbCxFU5ZXv2qZTjadvc4la65NqCSK+K/aI0QejkXXJMWN7GRj4RolG7Zkinga6vtAD/Dtkks98CJeTycbi8g2fHO4v5ONOmENdyqI7VuBKi1zEs8HkXnd/s9y871TPrMGmSgO0vd3smVb1DGJVV5rgFaKlhFexCOdbEyzreJ6EbGUp2dKFPGAxLGn2GyL/PX7QCcH/hXCGYl+sCIeyf04W5RKZT/LmRF72Q8OL+IRmVegMw5jddoi3p993jSI08cOi5HRBmCLUningsSJ02o96HleFHzjRTwcIYBmW6V3KtA+euI0G60SLEPDMlrkYEvtj1Dg48g4SFWC6oyK71SQiOEIiMyLNtuqv1NB/sSJBxK2JSZm6/ilFLkTJx5I6iXTbFtTlYxHOx5IrUV6MbYAbp9luRrsxIn7+uGBZfzYvGadEU9yCq8flnkbqTM6vZW0f8jrl9IZNRwFSwwSnoBff/MmZi9ISGg2KvO6aBJb+8QsYx4qgyKvn/a/Gq4zIv1oyOuXmZjV0SxUsBLoGFlcV8SXqU9nBK8PdQWCITKwLPpnF+qcmAUOf/fwRF/k9TdtYhZ7XEm4R15/8yZmcZvZyIHJvM2bmD08FRGP3MHyVBqMEJlXYmJWpXDI5fRgWAAkTv9tsll8eaB/DZ2Yzb1c7BQFOUa5a2JWIZfn39dNd9/ErEryNtzeESmhgOhoFjpQMOOm27ckhsM0XoxYsOSWvjO8iNd6MWLJmms7Q6QmZht2p8I1VbfEtVc1t5OW4Bolq5w0vVnoAN+u2Y/MwnEHWBGvoZ20hMg2vD6VUDoacTGi2Lx/4P4gUcTXKPNCABZCVYBEEa+pnbQEaKWokpMYxtTWTloCMfSRe6cCWsTD3Ub1gkl+pXxZoohHlJGawYtytuaRufZKf4RgkPg/ybKS850KaLNQsf+mCZDBM1b6LKMoDMOe/4w2C+mXebm4fax8tV82y80IexGfDWgnFXHb4S+LrosRJbnl8JdF90wozrWHvyzAxGyDuObwlwWYmG0W8oe/LNePoehD7vCXRXcRfy344S+LHpn3LohnyXe4PWiSee9Eong4oU3mvRu523ja/fs3oYMV8e3+/RsLPyvTK/NWA3Te2e7fv7kgmo3RL/NWBrc8ftA7E1o15Rm1RRNk3kphI0dDZN6KCcPV7wJsisxbNcSLvh6T1UT9OKQ+jncqmPn/19HR0dHR0dHR0dHR0ZGheMxfNyS2poZ7OLW25uoSe+jWWjS9veA+nIU1UvUjRM3AG1nq79vQymBs2dTkrYZQ27InJi9EZ5J5ODT5Mw2GmYdKf2ZJM2Rn7z1MeroNUUYvOXho13D/pB6Iax89TFT9pJtuouTkoeJrxLRBU/vXw5mv2xgl+LOzh/bGxO80OvbnnSZK5mb0juQJ53beQzs1rfvATW3WQzs2y0U3tose2qlJH2qY2mUP7bk52000t3ke2ptWNsOWoX6+y5mZzpu1r6G5DIlSZnCkMH+YuG2YmgAgPbfQF1uasEx2QWvbZogT7Ep9v5wZ0uGEBp5DSZv8JIQ63oBOONMi/CnZ8WixncZr3XbLE0+3ixG/+/5/NdSeuYB9shQAAAAASUVORK5CYII="
          />
          <Spin
            indicator={<LoadingOutlined style={{ fontSize: "2rem" }} spin />}
          />
        </Flex>
      )}
      {project.length > 0 && <CollapseSideBar project={project} />}
    </>
  );
}

export default App;
