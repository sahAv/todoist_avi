const token = "cfbee218350d84cdda8d595be6b9cb9284c9c30e";
import { TodoistApi } from "@doist/todoist-api-typescript";

export default function GetUserProjects() {
  const api = new TodoistApi(token);
  return api.getProjects();
}
